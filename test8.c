#include<stdio.h>
int swap(int *p,int *q)
{
    int temp;
    temp=*p;
    *p=*q;
    *q=temp;
}
int main()
{
    int num1,num2,*p,*q;
    printf("\n enter the 1st number=");
    scanf("%d",&num1);
    printf("\n Enter the 2nd number=");
    scanf("%d",&num2);
    p=&num1;
    q=&num2;
    swap(p,q);
    printf("\n num1=%d",*p);
    printf("\n num2=%d",*q);
    return 0;
}