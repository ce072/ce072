#include<stdio.h>
int main()
{
    int a[10][10],b[10][10],i,j,R,C;
    printf("\n enter the no. of rows and columns matrix A=");
    scanf("%d%d",&R,&C);
    for(i=0;i<R;i++)
    {
        for(j=0;j<C;j++)
        {
            printf("\n enter the %dth column in %dth row=",j,i);
            scanf("%d",&a[i][j]);
        }
    }
    printf("The matrix A=\n");
    for(i=0;i<R;i++)
    {
        for(j=0;j<C;j++)
        {
            printf("\t %d",a[i][j]);
        }
        printf("\n");
    }
    for(i=0;i<R;i++)
    {
        for(j=0;j<C;j++)
        {
            b[j][i]=a[i][j];
        }
    }
    printf("The transpose of A=\n");
    for(j=0;j<C;j++)
    {
        for(i=0;i<R;i++)
        {
            printf("\t %d",b[j][i]);
        }
        printf("\n");
    }
    return 0;
}